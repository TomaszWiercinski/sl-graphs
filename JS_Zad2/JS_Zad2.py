
from random import random
from time import perf_counter
import simple_graphs as sg
from graphs import Graph

COUNT = 100000  # Change this value depending on the speed of your computer
DATA = [(random() - 0.5) * 3 for _ in range(COUNT)]

e = 2.7182818284590452353602874713527

def sinh(x):
    return (1 - (e ** (-2 * x))) / (2 * (e ** -x))

def cosh(x):
    return (1 + (e ** (-2 * x))) / (2 * (e ** -x))

def tanh(x):
    tanh_x = sinh(x) / cosh(x)
    return tanh_x

def test(fn, name):
    start = perf_counter()
    result = fn(DATA)
    duration = perf_counter() - start
    print('{} took {:.3f} seconds\n\n'.format(name, duration))

    for d in result:
        assert -1 <= d <= 1, " incorrect values"

if __name__ == "__main__":
    # print('Running benchmarks with COUNT = {}'.format(COUNT))
    # test(lambda d: [tanh(x) for x in d], '[tanh(x) for x in d] (Python implementation)')
    # test(lambda d: [sg.fast_tanh(x) for x in d], '[tanh(x) for x in d] (C++ implementation)')

    edges_list = sg.EdgesList("DQc")
    graph = Graph("DQc")
    print(edges_list.number_of_vertices(), graph.number_of_vertices())
    print(edges_list.number_of_edges(), graph.number_of_edges())
    