#define PY_SSIZE_T_CLEAN
#define DEBUG
#include <Python.h>
#include <cmath>
#include <bitset>
#include <cstdlib>
#include <string>
#include <iostream>
#include <vector>

static PyObject* ErrorObject;

typedef struct {
    PyObject_HEAD
    PyObject* x_attr;        /* Attributes dictionary */
    std::bitset<64> vertices;
    std::vector<std::pair<int, int>> edges;
} EdgesListObject;

extern PyTypeObject EdgesList_Type;

#define EdgesListObject_Check(v) Py_IS_TYPE(v, &EdgesList_Type)

static EdgesListObject* newEdgesListObject(PyObject* arg) {
    EdgesListObject* self;
    self = PyObject_New(EdgesListObject, &EdgesList_Type);
    if (self == NULL)
        return NULL;
    self->x_attr = NULL;
    return self;
}

static EdgesListObject* EdgesListObject_new(PyTypeObject* subtype, PyObject* args, PyObject* kwargs) {
    EdgesListObject* rv;

    //if (!PyArg_ParseTuple(args, ":new"))
    //       return NULL;
    EdgesListObject* self = (EdgesListObject*)subtype->tp_alloc(subtype, 0);

    if (self != NULL) {
        self->vertices = 0;
        self->edges = {};
    }

    /*
    rv = newEdgesListObject(args);
    if (rv == NULL)
        return NULL;
    */

    return (EdgesListObject*)self;
}

static int get_bit_from_text(int n, const char* text) {
    int rem = n % 6;
    char c = *(text + ((n - rem) / 6));
    return c & (0b1 << rem);
}

static int EdgesListObject_init(EdgesListObject* self, PyObject* args, PyObject* kwargs) {
    const char* text;

    if (!PyArg_ParseTuple(args, "s", &text))
        return -1;

    int n = text[0] - 63;

    self->vertices = 0;
    for (int j = 0; j < n; j++) {
        self->vertices |= std::bitset<64>(0b1) << j;
    }

    int source = 0, target = 1;
    int i = 1;
    char curr = text[i];
    while (curr != '\0' && i < 100) {
        curr -= 63;
        // std::cout << std::bitset<8>(curr) << std::endl;
        for (int bit = 5; bit >= 0; bit--) {
            // printf("%d-%d: %d\n", source, target, (curr >> bit) & 0b1);
            if ((curr >> bit) & 0b1) {
                self->edges.push_back(std::make_pair((int)source, (int)target));
            }
            source++;
            if (source >= target) {
                source = 0;
                target++;
            }
            }
        i++;
        curr = text[i];
        }
    return 0;
}

static void EdgesList_dealloc(EdgesListObject* self) {
    Py_XDECREF(self->x_attr);
    PyObject_Free(self);
}

static PyObject* EdgesList_demo(EdgesListObject* self, PyObject* args) {
    if (!PyArg_ParseTuple(args, ":demo"))
        return NULL;
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject* EdgesList_number_of_vertices(EdgesListObject* self) {
    int vertex_count = 0;

    for (int i = 0; i < 64; i++)
        if (self->vertices[i])
            vertex_count++;
    
    //Py_INCREF(Py_NotImplemented);
    return PyLong_FromLong(vertex_count);
}

static PyObject* EdgesList_vertices(EdgesListObject* self) {
    PyObject* vertices = PySet_New(NULL);

    for (int i = 0; i < 64; i++) {
        if (self->vertices[i]) {
            PySet_Add(vertices, PyLong_FromLong(i));
        }
    }

    Py_INCREF(vertices);
    return vertices;
}

static PyObject* EdgesList_vertex_degree(EdgesListObject* self, PyObject* vertex) {
    int vertex_int, degree = 0;
    vertex_int = PyLong_AsLong(vertex);
    
    for (std::pair<int, int> edge : self->edges) {
        if (edge.first == vertex_int || edge.second == vertex_int) {
            degree++;
        }
    }

    // Py_INCREF(Py_NotImplemented);
    return PyLong_FromLong(degree);
}

static PyObject* EdgesList_vertex_neighbors(EdgesListObject* self, PyObject* vertex) {
    int vertex_int = PyLong_AsLong(vertex);
    PyObject* neighbors = PySet_New(NULL);

    for (std::pair<int, int> edge : self->edges) {
        if (edge.first == vertex_int) {
            PySet_Add(neighbors, PyLong_FromLong(edge.second));
        }
        else if (edge.second == vertex_int) {
            PySet_Add(neighbors, PyLong_FromLong(edge.first));
        }
    }

    //Py_INCREF(Py_NotImplemented);
    return neighbors;
}

static PyObject* EdgesList_add_vertex(EdgesListObject* self, PyObject* vertex) {
    int vertex_int = PyLong_AsLong(vertex);
    self->vertices |= (std::bitset<64>(1) << vertex_int);

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject* EdgesList_delete_vertex(EdgesListObject* self, PyObject* vertex) {
    int vertex_int = PyLong_AsLong(vertex);

#ifdef DEBUG
    printf("delete_vertex: Deleting vertex %d\n", vertex_int);
#endif // DEBUG

    self->vertices ^= (std::bitset<64>(1) << vertex_int);

    std::vector<std::vector<std::pair<int, int>>::iterator> edges_del;
    for (int i = 0; i < self->edges.size(); i++) {
        if (self->edges[i].first == vertex_int || self->edges[i].second == vertex_int) {
#ifdef DEBUG
            printf("delete_vertex: Also removing edge (%d, %d)\n", self->edges[i].first, self->edges[i].second);
#endif // DEBUG
            self->edges[i] = std::make_pair(-1, -1);
        }
    }

    self->edges.erase(remove(self->edges.begin(), self->edges.end(), std::make_pair(-1, -1)), self->edges.end());

#ifdef DEBUG
    printf("delete_vertex: Edges after changes: {");
    for (int i = 0; i+1 < self->edges.size(); i++) {
        printf("(%d, %d), ", self->edges[i].first, self->edges[i].second);
    }
    printf("(%d, %d)}\n", self->edges.back().first, self->edges.back().second);
#endif

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject* EdgesList_number_of_edges(EdgesListObject* self) {
    //Py_INCREF(Py_NotImplemented);
    return PyLong_FromLong(self->edges.size());
}

static PyObject* EdgesList_edges(EdgesListObject* self) {
    PyObject* edges = PySet_New(NULL);

    for (std::pair<int, int> edge : self->edges) {
        PyObject* edge_tuple = PyTuple_New(2);
        PyTuple_SetItem(edge_tuple, 0, PyLong_FromLong(edge.first));
        PyTuple_SetItem(edge_tuple, 1, PyLong_FromLong(edge.second));
        PySet_Add(edges, edge_tuple);
    }

    Py_INCREF(edges);
    return edges;
}

static PyObject* EdgesList_is_edge(EdgesListObject* self, PyObject* args) {
    int is_edge = 0;
    int source, target;

    if (!PyArg_ParseTuple(args, "ii", &source, &target))
        return NULL;

    if (source > target) {
        std::swap(source, target);
    }

    for (std::pair<int, int> edge : self->edges)
        if (edge.first == source && edge.second == target) {
            is_edge = 1;
            break;
        }

    // Py_INCREF(Py_NotImplemented);
    return PyBool_FromLong(is_edge);
}

static PyObject* EdgesList_add_edge(EdgesListObject* self, PyObject* args) {
    int source, target;

    if (!PyArg_ParseTuple(args, "ii", &source, &target))
        return NULL;

    if (source > target)
        std::swap(source, target);

#ifdef DEBUG
    printf("add_edge: Adding the edge (%d, %d)\n", source, target);
#endif // DEBUG

    self->edges.push_back(std::make_pair(source, target));

#ifdef DEBUG
    printf("add_edge: New edge list: {");
    for (int i = 0; i+1 < self->edges.size(); i++) {
        printf("(%d, %d), ", self->edges[i].first, self->edges[i].second);
    }
    printf("(%d, %d)}\n", self->edges.back().first, self->edges.back().second);
#endif // DEBUG

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject* EdgesList_delete_edge(EdgesListObject* self, PyObject* args) {
    int source, target;

    if (!PyArg_ParseTuple(args, "ii", &source, &target))
        return NULL;

    if (source > target)
        std::swap(source, target);

    for (std::vector<std::pair<int, int>>::iterator edge_it = self->edges.begin();
        edge_it != self->edges.end(); edge_it++) {
        if (*edge_it == std::make_pair(source, target)) {
            self->edges.erase(edge_it);
            break;
        }
    }

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject* EdgesList_smoothing(EdgesListObject* self, PyObject* args) {

    EdgesListObject* g = PyObject_New(EdgesListObject, &EdgesList_Type);
    if (g == NULL)
        return NULL;
    g->x_attr = NULL;
    g->vertices = self->vertices;
    g->edges = {};
    for (int i = 0; i < self->edges.size(); i++) {
#ifdef DEBUG
        printf("smoothing: Adding duplicate edge (%d, %d) to new graph\n", self->edges[i].first, self->edges[i].second);
#endif
        // g->edges.push_back(std::make_pair(0, 0));
    }
    g->edges.push_back(std::make_pair(0, 1));

    // g->edges = self->edges;

    /*
    g = Graph()
    for v in self.__vertices :
        g.add_vertex(v)
        for u, v in self.__edges :
            g.add_edge(u, v)
            vs = [v for v in g.vertices() if g.vertex_degree(v) == 2]
            while vs :
                u0, u1 = g.vertex_neighbors(vs[0])
                g.delete_vertex(vs[0])
                g.add_edge(u0, u1)
                vs = [v for v in g.vertices() if g.vertex_degree(v) == 2]
                return g
    */

    return (PyObject*)g;
}

static PyMethodDef EdgesList_methods[] = {
    {"demo", (PyCFunction)EdgesList_demo, METH_VARARGS, PyDoc_STR("demo() -> None")},
    {"number_of_vertices", (PyCFunction)EdgesList_number_of_vertices, 
        METH_NOARGS, PyDoc_STR("Returns the number of vertices of the graph.")},
    {"vertices", (PyCFunction)EdgesList_vertices,
        METH_NOARGS, PyDoc_STR("Returns the collection of graph vertices.")},
    {"vertex_degree", (PyCFunction)EdgesList_vertex_degree,
        METH_O, PyDoc_STR("Returns the degree of the given vertex.")},
    {"vertex_neighbors", (PyCFunction)EdgesList_vertex_neighbors,
        METH_O, PyDoc_STR("Returns a set of neighbors for a given vertex.")},
    {"add_vertex", (PyCFunction)EdgesList_add_vertex,
        METH_O, PyDoc_STR("Adds a given vertex to the graph.")},
    {"delete_vertex", (PyCFunction)EdgesList_delete_vertex,
        METH_O, PyDoc_STR("Deletes a vertex and its incident edges.")},
    {"number_of_edges", (PyCFunction)EdgesList_number_of_edges,
        METH_NOARGS, PyDoc_STR("Returns the number of edges of the graph.")},
    {"edges", (PyCFunction)EdgesList_edges,
        METH_NOARGS, PyDoc_STR("Returns a set of all edges if the graph.")},
    {"is_edge", (PyCFunction)EdgesList_is_edge,
        METH_VARARGS, PyDoc_STR("Checks wether two given vertices share an edge.")},
    {"add_edge", (PyCFunction)EdgesList_add_edge,
        METH_VARARGS, PyDoc_STR("Adds an edge between given vertices.")},
    {"delete_edge", (PyCFunction)EdgesList_delete_edge,
        METH_VARARGS, PyDoc_STR("Deletes an edge between given vertices.")},
    {"smoothing", (PyCFunction)EdgesList_smoothing, METH_NOARGS, PyDoc_STR(
        "Returns a graph in which all vertices of degree 2 have been replaced with an edge connecting their neighbors.")},
    {NULL, NULL} /* sentinel */
};

static PyObject* EdgesList_getattro(EdgesListObject* self, PyObject* name)
{
    if (self->x_attr != NULL) {
        PyObject* v = PyDict_GetItemWithError(self->x_attr, name);
        if (v != NULL) {
            Py_INCREF(v);
            return v;
        }
        else if (PyErr_Occurred()) {
            return NULL;
        }
    }
    return PyObject_GenericGetAttr((PyObject*)self, name);
}

static int EdgesList_setattr(EdgesListObject* self, const char* name, PyObject* v) {
    if (self->x_attr == NULL) {
        self->x_attr = PyDict_New();
        if (self->x_attr == NULL)
            return -1;
    }
    if (v == NULL) {
        int rv = PyDict_DelItemString(self->x_attr, name);
        if (rv < 0 && PyErr_ExceptionMatches(PyExc_KeyError))
            PyErr_SetString(PyExc_AttributeError,
                "delete non-existing EdgesList attribute");
        return rv;
    }
    else
        return PyDict_SetItemString(self->x_attr, name, v);
}

static PyTypeObject EdgesList_Type = {
    /* The ob_type field must be initialized in the module init function
     * to be portable to Windows without using C++. */
    PyVarObject_HEAD_INIT(NULL, 0)
    "simple_graphs.EdgesList",             /*tp_name*/
    sizeof(EdgesListObject),          /*tp_basicsize*/
    0,                          /*tp_itemsize*/
    /* methods */
    (destructor)EdgesList_dealloc,    /*tp_dealloc*/
    0,                          /*tp_vectorcall_offset*/
    (getattrfunc)0,             /*tp_getattr*/
    (setattrfunc)EdgesList_setattr,   /*tp_setattr*/
    0,                          /*tp_as_async*/
    0,                          /*tp_repr*/
    0,                          /*tp_as_number*/
    0,                          /*tp_as_sequence*/
    0,                          /*tp_as_mapping*/
    0,                          /*tp_hash*/
    0,                          /*tp_call*/
    0,                          /*tp_str*/
    (getattrofunc)EdgesList_getattro, /*tp_getattro*/
    0,                          /*tp_setattro*/
    0,                          /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT,         /*tp_flags*/
    0,                          /*tp_doc*/
    0,                          /*tp_traverse*/
    0,                          /*tp_clear*/
    0,                          /*tp_richcompare*/
    0,                          /*tp_weaklistoffset*/
    0,                          /*tp_iter*/
    0,                          /*tp_iternext*/
    EdgesList_methods,                /*tp_methods*/
    0,                          /*tp_members*/
    0,                          /*tp_getset*/
    0,                          /*tp_base*/
    0,                          /*tp_dict*/
    0,                          /*tp_descr_get*/
    0,                          /*tp_descr_set*/
    0,                          /*tp_dictoffset*/
    (initproc)EdgesListObject_init, /*tp_init*/
    0,                          /*tp_alloc*/
    (newfunc)EdgesListObject_new, //PyType_GenericNew,                          /*tp_new*/
    0,                          /*tp_free*/
    0,                          /*tp_is_gc*/
};
/* --------------------------------------------------------------------- */

/* Function of two integers returning integer */

PyDoc_STRVAR(simple_graphs_foo_doc,
    "foo(i,j)\n\
\n\
Return the sum of i and j.");

static PyObject* simple_graphs_foo(PyObject* self, PyObject* args)
{
    long i, j;
    long res;
    if (!PyArg_ParseTuple(args, "ll:foo", &i, &j))
        return NULL;
    res = i + j; /* XXX Do something here */
    return PyLong_FromLong(res);
}


/* Function of no arguments returning new EdgesList object */

static PyObject* simple_graphs_new(PyObject* self, PyObject* args)
{
    EdgesListObject* rv;

    if (!PyArg_ParseTuple(args, ":new"))
        return NULL;
    rv = newEdgesListObject(args);
    if (rv == NULL)
        return NULL;
    return (PyObject*)rv;
}

/* Example with subtle bug from extensions manual ("Thin Ice"). */

static PyObject* simple_graphs_bug(PyObject* self, PyObject* args)
{
    PyObject* list, * item;

    if (!PyArg_ParseTuple(args, "O:bug", &list))
        return NULL;

    item = PyList_GetItem(list, 0);
    /* Py_INCREF(item); */
    PyList_SetItem(list, 1, PyLong_FromLong(0L));
    PyObject_Print(item, stdout, 0);
    printf("\n");
    /* Py_DECREF(item); */

    Py_INCREF(Py_None);
    return Py_None;
}

/* Test bad format character */

static PyObject* simple_graphs_roj(PyObject* self, PyObject* args)
{
    PyObject* a;
    long b;
    if (!PyArg_ParseTuple(args, "O#:roj", &a, &b))
        return NULL;
    Py_INCREF(Py_None);
    return Py_None;
}


/* ---------- */

static PyTypeObject Str_Type = {
    /* The ob_type field must be initialized in the module init function
     * to be portable to Windows without using C++. */
    PyVarObject_HEAD_INIT(NULL, 0)
    "simple_graphs.Str",             /*tp_name*/
    0,                          /*tp_basicsize*/
    0,                          /*tp_itemsize*/
    /* methods */
    0,                          /*tp_dealloc*/
    0,                          /*tp_vectorcall_offset*/
    0,                          /*tp_getattr*/
    0,                          /*tp_setattr*/
    0,                          /*tp_as_async*/
    0,                          /*tp_repr*/
    0,                          /*tp_as_number*/
    0,                          /*tp_as_sequence*/
    0,                          /*tp_as_mapping*/
    0,                          /*tp_hash*/
    0,                          /*tp_call*/
    0,                          /*tp_str*/
    0,                          /*tp_getattro*/
    0,                          /*tp_setattro*/
    0,                          /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags*/
    0,                          /*tp_doc*/
    0,                          /*tp_traverse*/
    0,                          /*tp_clear*/
    0,                          /*tp_richcompare*/
    0,                          /*tp_weaklistoffset*/
    0,                          /*tp_iter*/
    0,                          /*tp_iternext*/
    0,                          /*tp_methods*/
    0,                          /*tp_members*/
    0,                          /*tp_getset*/
    0, /* see PyInit_xx */      /*tp_base*/
    0,                          /*tp_dict*/
    0,                          /*tp_descr_get*/
    0,                          /*tp_descr_set*/
    0,                          /*tp_dictoffset*/
    0,                          /*tp_init*/
    0,                          /*tp_alloc*/
    0,                          /*tp_new*/
    0,                          /*tp_free*/
    0,                          /*tp_is_gc*/
};

/* ---------- */

static PyObject* null_richcompare(PyObject* self, PyObject* other, int op)
{
    Py_INCREF(Py_NotImplemented);
    return Py_NotImplemented;
}

static PyTypeObject Null_Type = {
    /* The ob_type field must be initialized in the module init function
     * to be portable to Windows without using C++. */
    PyVarObject_HEAD_INIT(NULL, 0)
    "simple_graphs.Null",            /*tp_name*/
    0,                          /*tp_basicsize*/
    0,                          /*tp_itemsize*/
    /* methods */
    0,                          /*tp_dealloc*/
    0,                          /*tp_vectorcall_offset*/
    0,                          /*tp_getattr*/
    0,                          /*tp_setattr*/
    0,                          /*tp_as_async*/
    0,                          /*tp_repr*/
    0,                          /*tp_as_number*/
    0,                          /*tp_as_sequence*/
    0,                          /*tp_as_mapping*/
    0,                          /*tp_hash*/
    0,                          /*tp_call*/
    0,                          /*tp_str*/
    0,                          /*tp_getattro*/
    0,                          /*tp_setattro*/
    0,                          /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags*/
    0,                          /*tp_doc*/
    0,                          /*tp_traverse*/
    0,                          /*tp_clear*/
    null_richcompare,           /*tp_richcompare*/
    0,                          /*tp_weaklistoffset*/
    0,                          /*tp_iter*/
    0,                          /*tp_iternext*/
    0,                          /*tp_methods*/
    0,                          /*tp_members*/
    0,                          /*tp_getset*/
    0, /* see PyInit_xx */      /*tp_base*/
    0,                          /*tp_dict*/
    0,                          /*tp_descr_get*/
    0,                          /*tp_descr_set*/
    0,                          /*tp_dictoffset*/
    0,                          /*tp_init*/
    0,                          /*tp_alloc*/
    PyType_GenericNew,          /*tp_new*/
    0,                          /*tp_free*/
    0,                          /*tp_is_gc*/
};


/* ---------- */

PyObject* read_g6(PyObject* self, PyObject* args) {
    const char* text;

    if (!PyArg_ParseTuple(args, "s", &text))
        return NULL;

    printf("n = %d\n", text[0] - 63);

    int source = 0, target = 1;
    int i = 1;
    char curr = text[i];
    while (curr != '\0' && i < 100) {
        curr -= 63;
        std::cout << std::bitset<8>(curr) << std::endl;
        for (int bit = 5; bit >= 0; bit--) {
            printf("%d-%d: %d\n", source, target, (curr >> bit) & 0b1);
            source++;
            if (source >= target) {
                source = 0;
                target++;
            }
        }
        i++;
        curr = text[i];
    }

    return Py_None;
}

/* List of functions defined in the module */

static PyMethodDef simple_graphs_methods[] = {
    {"roj", simple_graphs_roj, METH_VARARGS, PyDoc_STR("roj(a,b) -> None")},
    {"foo", simple_graphs_foo, METH_VARARGS, simple_graphs_foo_doc},
    {"new", simple_graphs_new, METH_VARARGS, PyDoc_STR("new() -> new Xx object")},
    {"bug", simple_graphs_bug, METH_VARARGS, PyDoc_STR("bug(o) -> None")},
    { "read_graph", (PyCFunction)read_g6, METH_VARARGS, PyDoc_STR("Reads the fucking g6 format or someshit")},
    {NULL, NULL}           /* sentinel */
};

PyDoc_STRVAR(module_doc, "This is a template module just for instruction.");

static int simple_graphs_exec(PyObject* m)
{
    /* Slot initialization is subject to the rules of initializing globals.
       C99 requires the initializers to be "address constants".  Function
       designators like 'PyType_GenericNew', with implicit conversion to
       a pointer, are valid C99 address constants.
       However, the unary '&' operator applied to a non-static variable
       like 'PyBaseObject_Type' is not required to produce an address
       constant.  Compilers may support this (gcc does), MSVC does not.
       Both compilers are strictly standard conforming in this particular
       behavior.
    */

    EdgesList_Type.tp_base = &PyBaseObject_Type;
    Null_Type.tp_base = &PyBaseObject_Type;
    Str_Type.tp_base = &PyUnicode_Type;

    /* Finalize the type object including setting type of the new type
     * object; doing it here is required for portability, too. */
    if (PyType_Ready(&EdgesList_Type) < 0)
        goto fail;

    /* Add some symbolic constants to the module */
    if (ErrorObject == NULL) {
        ErrorObject = PyErr_NewException("simple_graphs.error", NULL, NULL);
        if (ErrorObject == NULL)
            goto fail;
    }
    Py_INCREF(ErrorObject);
    PyModule_AddObject(m, "error", ErrorObject);
    PyModule_AddObject(m, "EdgesList", (PyObject*)&EdgesList_Type);

    /* Add Str */
    if (PyType_Ready(&Str_Type) < 0)
        goto fail;
    PyModule_AddObject(m, "Str", (PyObject*)&Str_Type);

    /* Add Null */
    if (PyType_Ready(&Null_Type) < 0)
        goto fail;
    PyModule_AddObject(m, "Null", (PyObject*)&Null_Type);

    return 0;
fail:
    Py_XDECREF(m);
    return -1;
}

static struct PyModuleDef_Slot simple_graphs_slots[] = {
    {Py_mod_exec, simple_graphs_exec},
    {0, NULL},
};

static struct PyModuleDef simple_graphs_module = {
    PyModuleDef_HEAD_INIT,
    "simple_graphs",
    module_doc,
    0,
    simple_graphs_methods,
    simple_graphs_slots,
    NULL,
    NULL,
    NULL
};

PyMODINIT_FUNC PyInit_simple_graphs(void)
{
    return PyModuleDef_Init(&simple_graphs_module);
}